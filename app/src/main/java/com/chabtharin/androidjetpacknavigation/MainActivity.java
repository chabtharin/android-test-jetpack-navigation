package com.chabtharin.androidjetpacknavigation;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.chabtharin.androidjetpacknavigation.databinding.ActivityMainBinding;
import com.google.android.material.navigation.NavigationBarView;

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding binding;
    private NavController navController;
    private View view;
    private int selectedFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        view = binding.getRoot();
        setContentView(view);
        NavHostFragment navHostFragment = (NavHostFragment) getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);
        navController = navHostFragment.getNavController();
        selectedFragment = R.id.home;
        binding.bottomNavigationView.setOnItemSelectedListener(new NavigationBarView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.home:
                        return handleChangeFragment(R.id.home, R.id.notification_to_home);
                    case R.id.notification:
                        return handleChangeFragment(R.id.notification, R.id.home_to_notification);
                }
                return false;
            }
        });
    }

    private boolean handleChangeFragment(int itemId, int navigation) {
        if (selectedFragment != itemId) {
            navController.navigate(navigation);
            selectedFragment = itemId;
            return true;
        }
        return false;
    }
}